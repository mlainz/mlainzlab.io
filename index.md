---
layout: single
author_profile: true
permalink: /
title: "About me"
---

Currently writing my PHD thesis at ICMAT, supervised by Prof. Manuel de León. I studied Mathematics and Physics in Universidad de Cantabria, including a one year stay in Princeton University, where I learned about critical phenomena in planar models and other related topics in statistical mechanics. After that, I moved to Madrid and I attended to the master of Mathematics at UAM.

My research lies in differential geometry and its application to physics and other sciences. My PHD deals with Contact Hamiltonian systems and how they can be used to model irreversible dynamics. We develop tools for understanding and controlling the behavior of thermodynamic systems and mechanical systems with friction.