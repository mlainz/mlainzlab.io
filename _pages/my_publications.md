---
title: Publications
permalink: /publications/
toc: true
---
## In journals
{% bibliography %}

## Preprints
{% bibliography --file my_preprints %}
